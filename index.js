const spawn = require('child_process').spawn
const commandExists = require('command-exists').sync;
let gitClone = null;

const gitShallowClone = (gitURL, localPath, depth, callback) => {
    if (!gitURL) {
        throw new Error('Please provide the repo url to clone');
    } else if (!depth || depth < 1) {
        throw new Error('Please provide the depth to clone');
    }

    let args = ['clone', '--depth', depth, gitURL];

    if (localPath) args.push(localPath);


    /**
    WARNING: 'git lfs clone' is deprecated and will not be updated
        with new flags from 'git clone'

    'git clone' has been updated in upstream Git to have comparable
        speeds to 'git lfs clone'.
     */

    if (commandExists('')) { // what is the correct command to check for, `git lfs` ?
        console.log('Cloning using: git lfs clone')
        args.unshift('lfs')
    }

    gitClone = spawn('git', args);

    gitClone.stdout.on('data', (data) => {
        console.log(data.toString());
    });

    gitClone.stderr.on('data', (data) => {
        console.log(data.toString())
    });

    gitClone.on('close', (status) => {
        if (status === 0) {
            callback();
        } else {
            callback(new Error(`'git clone' failed with status ${status}`))
        }
    })
}

gitShallowClone('https://github.com/torvalds/linux.git', './linux', 1, () => {
    console.log('DONE')
})